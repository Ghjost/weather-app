// Weatherapi.com
const WA_BASE_URL = "http://api.weatherapi.com/v1/forecast.json";
const WA_API_KEY = "key=YOUR_KEY";
const WA_BASE_UNIT = "units=metric";
const WA_BASE_LANG = "lang=fr";
const WA_FORCAST_LENGTH = "days=3";

// GeoApi
const GO_A_BASE_URL = "https://api.geoapify.com/v1/geocode";
const GO_A_API_KEY = "apiKey=YOUR_KEY";
const GO_A_METHOD_AUTOCOMPLETE = "autocomplete";

let searchForm = document.getElementById("search-div");
let submitFormButton = document.getElementById("submit-search-form");
let searchQueryForm = document.getElementById("search-query");
let searchQuery = "";

submitFormButton.addEventListener("click", async (event) => {
  event.preventDefault;
  updateOnChangeOrSubmit();
});
searchQueryForm.addEventListener("input", async (event) => {
  event.preventDefault;
  updateOnChangeOrSubmit();
});

const updateOnChangeOrSubmit = async () => {
  searchQuery = document.getElementById("search-query").value;
  console.log(await buildBaseUrlGoA());

  const complete = await buildBaseUrlGoA();
  if (complete !== null && complete.status === 200) {
    addResultElements(complete.data);
  } else {
    console.log("cpt");
  }
};

const addResultElements = (results) => {
  const searchResultList = document.getElementById("search-result");
  displaySearchResultDiv(true);
  searchResultList.classList.add("search-result");

  searchResultList
    .querySelectorAll(".search-result-item")
    .forEach((element) => {
      element.remove();
    });
  searchResultList
    .querySelectorAll(".search-result-item-disabled")
    .forEach((element) => {
      element.remove();
    });

  if (results?.features.length === 0) {
    const resultItem = document.createElement("div");
    resultItem.classList.add("search-result-item-disabled");
    resultItem.id = "search-result-item-disabled";

    const resultItemSpan = document.createElement("span");
    resultItemSpan.innerHTML = "Aucun resultat disponible pour votre recherche";

    resultItem.appendChild(resultItemSpan);
    searchResultList.appendChild(resultItem);
  } else {
    results.features.forEach((element) => {
      const resultItem = document.createElement("div");
      resultItem.classList.add("search-result-item");

      const resultItemSpan = document.createElement("span");
      resultItemSpan.innerHTML = element.properties.formatted;
      // element.properties.address_line1 +
      // ", " +
      // element.properties.county +
      // ", " +
      // element.properties.country;

      resultItem.appendChild(resultItemSpan);
      searchResultList.appendChild(resultItem);

      resultItem.addEventListener("click", (event) => {
        selectCityResultitem(
          element.properties.lat + "," + element.properties.lon,
          element
        );
      });
    });
  }
};

const selectCityResultitem = async (data, location) => {
  // console.log(data);
  searchQuery = data;
  const weatherData = await buildBaseUrlWA();
  displaySearchResultDiv(false);

  // console.log(weatherData);

  displayCardResult(true);

  console.log(location);

  const title = document.getElementById("title");

  const feelslike_c = document.getElementById("feelslike_c");
  const temp_c = document.getElementById("temp_c");
  const humidity = document.getElementById("humidity");
  const wind_kph = document.getElementById("wind_kph");
  const wind_degree = document.getElementById("wind_degree");

  const iconWeather = document.getElementById("icon-weather");
  const textWeather = document.getElementById("text-weather");

  const forecastContainer = document.getElementById("forecast");

  console.log(weatherData.data.forecast);

  title.innerHTML = "Metéo actuelle à " + location.properties.formatted;

  feelslike_c.innerHTML = weatherData.data.current.feelslike_c + "°C";
  temp_c.innerHTML = weatherData.data.current.temp_c + "°C";
  humidity.innerHTML = weatherData.data.current.humidity + "%";
  wind_kph.innerHTML = weatherData.data.current.wind_kph + " Km/H";
  wind_degree.innerHTML =
    weatherData.data.current.wind_degree +
    "° (" +
    weatherData.data.current.wind_dir +
    ")";

  iconWeather.src = "https://" + weatherData.data.current.condition.icon;
  textWeather.innerHTML = weatherData.data.current.condition.text;

  Object.values(weatherData.data.forecast)[0].forEach((element) => {
    console.log(element);
    const item = document.createElement("div");
    item.classList.add("item");

    const date = document.createElement("span");
    date.innerHTML = element.date;
    date.classList.add("date");

    const iconContainer = document.createElement("span");
    iconContainer.classList.add("icon-container");

    const img = document.createElement("img");
    img.src = "https://" + element.day.condition.icon;

    const text = document.createElement("span");
    text.innerHTML = element.day.condition.text;
    item.classList.add("text");

    const minTemp = document.createElement("span");
    minTemp.innerHTML = element.day.mintemp_c;
    minTemp.classList.add("min-temp");

    const maxTemp = document.createElement("span");
    maxTemp.innerHTML = element.day.maxtemp_c;
    maxTemp.classList.add("max-temp");

    const avgTemp = document.createElement("span");
    avgTemp.innerHTML = element.day.avgtemp_c;
    avgTemp.classList.add("avg-temp");

    iconContainer.appendChild(img);

    item.appendChild(date);
    item.appendChild(iconContainer);
    item.appendChild(text);
    item.appendChild(minTemp);
    item.appendChild(maxTemp);
    item.appendChild(avgTemp);

    forecastContainer.appendChild(item);
  });
};

// Weather Api
const buildBaseUrlWA = () => {
  let url =
    WA_BASE_URL +
    "?" +
    WA_API_KEY +
    "&q=" +
    searchQuery +
    "&" +
    WA_BASE_LANG +
    "&" +
    WA_FORCAST_LENGTH;
  console.log(url);

  const responseData = axios
    .get(url)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log(error);
      return null;
    });

  return responseData;
};

// Autocomplete
const buildBaseUrlGoA = () => {
  let url =
    GO_A_BASE_URL +
    "/" +
    GO_A_METHOD_AUTOCOMPLETE +
    "?text=" +
    searchQuery +
    "&" +
    GO_A_API_KEY;
  console.log(encodeURI(url));

  const responseData = axios
    .get(encodeURI(url))
    .then((response) => {
      return response;
    })
    .catch((error) => {
      // console.log(error);
      return null;
    });

  return responseData;
};

// Help

const displaySearchResultDiv = (active) => {
  const searchResultList = document.getElementById("search-result");
  searchResultList.classList.add("search-result");

  if (active === true) {
    searchResultList.classList.add("search-result");
    searchResultList.removeAttribute("hidden");
  }

  if (active === false) {
    searchResultList.classList.remove("search-result");
    searchResultList.setAttribute("hidden", "hidden");
  }
};

const displayCardResult = (active) => {
  const cardResultDiv = document.getElementById("card-result");
  cardResultDiv.classList.add("card-result", "card-background");

  if (active === true) {
    cardResultDiv.classList.add("card-result");
    cardResultDiv.removeAttribute("hidden");
  }

  if (active === false) {
    cardResultDiv.classList.remove("card-result");
    cardResultDiv.setAttribute("hidden", "hidden");
  }
};
